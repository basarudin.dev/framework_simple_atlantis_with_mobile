<?php
    /*
    |--------------------------------------------------------------------------
    | Route
    |--------------------------------------------------------------------------
    |Routing dari parameter URI
    |Khusus untuk modul login menyertakan modul firewall.php,selain modul login menggunakan modul secure.php
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string

    |masih belum memanfaatkan htaccess
    |variable standart :
    |   page -> digunakan untuk mengakses halaman
    |   type -> 0 = view, 1 = model ; 0 default
    |   action -> list,detail,edit,delete
    |   record_id -> string untuk menujukan id dari primary key table
    */

    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.user.php");
    $oUserInfo = new UserInfo();

    //default adalah login
    include_once($SYSTEM['DIR_MODUL_CORE']."/firewall.php");
    //jika sudah login
    if(isset($_POST['TOKEN_KEY']))
    {
        if(isset($_REQUEST['page']))
        {
            //-----------------halaman yang hanya boleh diakses yang sudah login-----------------
            switch ($_REQUEST['page']) 
            {
                case 'admin':
                    require_once($SYSTEM['DIR_MODUL']."/admin/admin.php");
                    break;
                case 'app':
                    require_once($SYSTEM['DIR_MODUL_CORE']."/application.php");
                    break;
            }//end switch   
        }
        else
        {
            $_REQUEST['error_code']  = "tidak mempunyai halaman";
            require_once($SYSTEM['DIR_MODUL_CORE']."/denied.php");
        }
        
    }
    //jika belum login
    else
    {

        if(isset($_REQUEST['page']))
        {
            //-----------------halaman yang hanya boleh diakses yang sudah login-----------------
            switch ($_REQUEST['page']) 
            {
                case 'login':
                    require_once($SYSTEM['DIR_MODUL']."/login/login.php");
                    break;
                case 'app':
                    require_once($SYSTEM['DIR_MODUL_CORE']."/application.php");
                    break;
            }//end switch  
        }
        else
        {
            $_REQUEST['error_code']  = "tidak mempunyai halaman";
            require_once($SYSTEM['DIR_MODUL_CORE']."/denied.php");
        }
    }
?>