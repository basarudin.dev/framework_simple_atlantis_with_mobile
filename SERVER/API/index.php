<?php
    /*
    |--------------------------------------------------------------------------
    | Index
    |--------------------------------------------------------------------------
    |Halaman utama 
    |Berfungsi sebagai pemaggil init, route sekaligus autoload dari fungsi fungsi
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    |
    */
    session_start();

    $respone['status'] = "error";
    $respone['desc'] = "";
    
	include_once("config.php");
    include_once($SYSTEM['DIR_MODUL_CORE']."/init.php");
    include_once($SYSTEM['DIR_MODUL_CORE']."/route.php");

    if(isset($respone))
    {
        echo json_encode($respone);
    }
?>