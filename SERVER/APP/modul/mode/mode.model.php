<?PHP
     /**
      * transaksi.svr.php
      * 
      * File ini digunakan untuk administrasi transaksi
      *  
      *
      * @version         1.0
      * @author          basarudin
      * @created     feb 17 ,2013
      * @log
      *        - 24 maret 2014 create new file
      *
      * prefix parameter:
      *    n  - node
      *    o  - object
      *    a  - array
      *    s  - string
      *    b  - boolean
      *    f  - float
      *    i  - integer
      *    uk  - unknown
      *    fn - function
      *    _  - parameter
      **/
    $PAGE_ID = "MDE100";
    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");

    $oUser = new UserInfo();

    $a_errors = array();

    $respone['status'] = "error";
    $respone['desc'] = "";

    $s_error  = "";


     
    if(isset($_REQUEST['action']))
    {


        if($_REQUEST['action'] == 'theme')
        {
            if($_REQUEST['theme_id'] == "")
            {
                $a_errors[] = "tidak ada tema";
            }
            if (!$a_errors) 
            {        
                if($oUser->setTheme($USER[0]['userID'],$_REQUEST['theme_id']))
                {

                    $respone['status'] = 'success';
                    $respone['desc'] = " Tema berhasil diubah";
                }
                else
                {
                    $respone['status'] = 'error';
                    $respone['desc'] = " Tema tidak dapat diubah";
                }
                    
            }
            else
            {
                $s_error =  '';
                foreach ($a_errors as $error) {
                    $s_error .= "$error<br />";
                }
                $respone['status'] = 'error';
                $respone['desc'] = $s_error;
            }
            echo json_encode($respone);

        }


          
     }
     $oUser->closeDB();

?>