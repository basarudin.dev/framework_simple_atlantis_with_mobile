<?php
    /*
    |--------------------------------------------------------------------------
    | wilayah view
    |--------------------------------------------------------------------------
    |view  modul wilayah 
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */



    $JS_EXTENDED .= "
                    
                    <!-- Datatables -->
                        <script src='modul/{$MODUL}/{$MODUL}.js'></script>
                    ";
    $CSS_EXTENDED .= "
                        <link rel='stylesheet' href='modul/{$MODUL}/{$MODUL}.css'>
                    ";



    $TITLE_MAIN = "Selamat Datang ";
    $TITLE_SUB = "";


    $CONTENT_MAIN = "
                

<!-- BEGIN PAGE CONTENT -->


        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>
                        <div id='myCarousel' class='carousel slide' data-ride='carousel'>
                            <div class='carousel-inner'>
                                <div class='carousel-item active'>
                                    <div class='item item1 active' >
                                        <div class='fill'>
                                            <div class='inner-content '>
                                                <div class='row'>
                                                    <div class='col-md-4'>

                                                        <div class='carousel-img'>
                                                            <img src='assets/img/slider/presenter.png' alt='sofa' class='w-100 img img-responsive' />
                                                        </div>
                                                    </div>
                                                    <div class='col-md-8'>
                                                        <div class='row h-100'>
                                                            <div class='carousel-desc col-sm-12 my-auto'>

                                                                <h2  mx-auto'>Selamat datang  {$USER[0]['panggilan']}</h2>
                                                                <p  mx-auto'>Selamat berkarya, jadikan perkerjaan anda berkah. Kontribusi anda akan sangat bermanfaat bagi perusahaan. Semoga aktifitas kita hari ini dicatat sebagai amal kebaikan. amien</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                                <div class='carousel-item'>

                                    <div class='item item2 active' >
                                        <div class='fill'>
                                            <div class='inner-content '>
                                                <div class='row'>
                                                    <div class='col-md-12'>

                                                        <div class='carousel-img'>
                                                            <img src='assets/img/slider/app.png' alt='sofa' class='img img-responsive' class='w-100 img img-responsive'/>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-12'>
                                                        <div class='row h-100'>
                                                            <div class='carousel-desc col-sm-12 my-auto'>

                                                                

                                                                <h2>Simple Usefull Powerfull</h2>
                                                                <p>Sistem ini dibuat sesederhana mungkin yang ditujukan untuk membantu perkerjaan administrasi komputer dan layanan helpdesk. </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                            </div>
                                        </div>  
                                    </div>
                                    
                                </div>
                                <div class='carousel-item'>

                                    
                                    <div class='item item3 active' >
                                        <div class='fill'>
                                            <div class='inner-content '>
                                                <div class='row'>
                                                    <div class='col-md-12'>

                                                        <div class='carousel-img'>
                                                            <img src='assets/img/slider/helpdesk.png' alt='sofa' class='img img-responsive' class='w-100 img img-responsive'/>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-12'>
                                                        <div class='row h-100'>
                                                            <div class='carousel-desc col-sm-12 my-auto'>

                                                                <h2>Anda Butuh Bantuan?</h2>
                                                                <p>Silahkan menghubungi administrator jika anda mengalami kesulitan dalam mengoperasikan aplikasi sistem ini.</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                            </div>
                                        </div>  
                                    </div>
                                    
                                </div>

                                <a class='carousel-control-prev' href='#myCarousel' role='button' data-slide='prev'>
                                    <span class='carousel-control-prev-icon' aria-hidden='true'></span>
                                    <span class='sr-only'>Previous</span>
                                </a>
                                <a class='carousel-control-next' href='#myCarousel' role='button' data-slide='next'>
                                    <span class='carousel-control-next-icon' aria-hidden='true'></span>
                                    <span class='sr-only'>Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- END PAGE CONTENT -->
              ";
?>