<?php
    /*
    |--------------------------------------------------------------------------
    | wilayah view
    |--------------------------------------------------------------------------
    |view  modul wilayah 
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    $oWilayah = new Wilayah();


    $a_title[] = "KODE WILAYAH";
    $a_title[] = "NAMA WILAYAH";
    $a_title[] = "PARENT";
    $a_title[] = "";
    $a_title_class[] = " style='width:100px;' ";
    $a_title_class[] = " style='width:150px;' ";
    $a_title_class[] = " style='width:200px;' ";
    $a_title_class[] = " style='width:10px;' ";

    $s_table_container = "";
    $s_condition = " WHERE TRUE";
    $s_limit = "  ";
    $s_order = "  ORDER BY `idWilayah` ASC ";


    $JS_EXTENDED .= "
                    
                    <!-- Datatables -->
                    <script src='assets/js/plugin/datatables/datatables.min.js'></script>
                    <script src='modul/{$MODUL}/{$MODUL}.js'></script>
                    ";
    $CSS_EXTENDED .= "
                    ";


    $a_data = $oWilayah->getList($s_condition, $s_order, $s_limit);
    if(isset($a_data))
    {
          
        $s_table_container ="";
        $s_table_container .="
                    <table  id='table-wilayah' class='display table table-striped table-hover'   width='100%' >
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
        $s_table_container .="</tr>
                        </thead>";
        $s_table_container .="<tbody>";

        for($i=0;$i<count($a_data);$i++)
        {
            $s_status = "";
            $s_button = "";
            $s_bg_column = "";

            $s_condition = " WHERE parentWilayah = '{$a_data[$i]['idWilayah']}'; " ;
            if($oWilayah->getCount($s_condition)==0)
            {
                $s_button = "<button class='button-wilayah-delete btn btn-flat   btn-danger btn-sm '  record-id='{$a_data[$i]['idWilayah']}'  style='margin-left: 2px;'>Hapus</button>";
            }

                    
            //untuk informasi jumlah soal
            $s_table_container .="<tr >";
            $s_table_container .= "<td  align='left'>"
                                     .strtoupper ($a_data[$i]['idWilayah'])
                                ."</td>";
            $s_table_container .= "<td  align='left'  class='wilayah-name'>"
                                     .strtoupper ($a_data[$i]['namaWilayah'])
                                ."</td>";
            $s_table_container .= "<td  align='left' >"
                                     .strtoupper ($a_data[$i]['namaParent'])
                                ."</td>";
            $s_table_container .= "<td  > $s_button</td>";
            $s_table_container .="</tr>";
        }    
        $s_table_container .="</tbody>";
        $s_table_container .="</table>";
    }




    $TITLE_MAIN = "Wilayah Kerja";
    $TITLE_SUB = "";

    $BUTTON_ACTION = "
                                <button class='button-wilayah-create  btn btn-success  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-plus'></i>
                                    </span>
                                    Tambah Wilayah
                                </button>
    ";

    $CONTENT_MAIN = "
                

<!-- BEGIN PAGE CONTENT -->


        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>

                        <div class='col-md-12'>
                            <div class='card'>
                                <div class='card-header'>
                                    <div class='card-title'>Wilayah Kerja</div>
                                    <div class='card-category'>table wilayah kerja</div>
                                </div>
                                <div class='card-body'>
                                    <div class='table-responsive'>
                                        {$s_table_container}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- END PAGE CONTENT -->
              ";
    $oWilayah->closeDB();

?>