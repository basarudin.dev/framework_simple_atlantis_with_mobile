<?php
/*
|--------------------------------------------------------------------------
| Login View
|--------------------------------------------------------------------------
|
|View login
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

$JS_EXTENDED .= "
                <script src='assets/js/plugin/jquery-validation/js/jquery.validate.js'></script>
                <script src='modul/login/login.js'></script>
                ";
require_once($SYSTEM['DIR_MODUL_LAYOUT'].'/meta.php');
require_once($SYSTEM['DIR_MODUL_LAYOUT'].'/css.php');
require_once($SYSTEM['DIR_MODUL_LAYOUT'].'/js.php');

$LAYOUT_CSS .= "
                <link rel='stylesheet' href='modul/login/login.css'>
";
    $CONTENTS .= "
    <!DOCTYPE html>
<html>
<head>
";
    $CONTENTS .= $LAYOUT_META;
    $CONTENTS .= $LAYOUT_CSS;
    $CONTENTS .= "
</head>
<body class='hold-transition login-page'>

    <div class='login-box'>
        <!-- /.login-logo -->
        <div class='login-box-body'>
            <div class='login-logo'>
                <b>{$SYSTEM['APP_NAME']}</b>
            </div>
            <p class='login-box-msg'>Silahkan login untuk memulai</p>

            <form action='asset/index2.html' method='post' class='login-form'>

                <div class='alert alert-danger display-hide' style='display:none'>
                    <button class='close' data-close='alert'></button>
                    <span  id='info'>
                    </span>
                </div>
                <div class='form-group'>
                    <div class='input-icon'>
                        <input class='form-control' placeholder='Username' name='username' type='text'>
                        <span class='input-icon-addon'>
                            <i class='fa fa-user'></i>
                        </span>
                    </div>
                </div>
                <div class='form-group'>
                    <div class='input-icon'>
                        <input type='password'  class='form-control' placeholder='Password' name='password' type='text'>
                        <span class='input-icon-addon'>
                            <i class='fa fas fa-key'></i>
                        </span>
                    </div>
                </div>
                
                <div class='row'>
                    <div class='col-sm-8'>
                    </div>
                    <!-- /.col -->
                    <div class='col-sm-4'>
                        <button type='submit' class='btn btn-success btn-block btn-flat'>Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>

";

    $CONTENTS .= $LAYOUT_JS;

    $CONTENTS .= '
</body>
</html>
    ';
?>