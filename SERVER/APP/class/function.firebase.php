<?php



	//rekursive notification
	function cron_send_notification ($a_firebase_data,$system= array(),$increment = 0,$checksum = array())
	{
		require_once($system['DIR_PATH']."/class/class.notification.php");
		$oNotif = new Notification();
		if(!isset($checksum['total']))
		{
			$checksum['total'] = count($a_firebase_data); 
		}
		if(!isset($checksum['success']))
		{
			$checksum['success'] = 0; 
		}
		if(!isset($checksum['error']))
		{
			$checksum['error'] = 0; 
		}


		$url = $system['FIREBASE_URL'];
		$headers = $system['FIREBASE_HEADER'];
		if(count($a_firebase_data) > $increment )
		{
			$a_tokens[0] = $a_firebase_data[$increment]['notif-to-firebase-id'];
			$tokens = $a_tokens;
			//from sistem format to firebase format
			$message['android_channel_id'] = $a_firebase_data[$increment]['notif-id'];
			$message['body'] = $a_firebase_data[$increment]['notif-body'];
			$message['title'] = $a_firebase_data[$increment]['notif-title'];
			$fields = array(
				 'registration_ids' => $tokens,
				 /*
				 'notification' => [
									"body" => $message['message'],
									"notifID" => $message['notifID']
							],
							*/
				 'data' => $message
				);

			//var_dump($fields);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, $system['FIREBASE_DELAY']);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			$result = curl_exec($ch);   
			if ($result === FALSE) {
				die('Curl failed: ' . curl_error($ch));

		        $checksum['error']++;
		        $checksum['error_notif_id'][] = $a_firebase_data[$increment]['notif-id'];
				$oNotif->update($a_firebase_data[$increment]['notif-id'],$system['FIREBASE_FLAG_ERROR']);
			}
			else
			{
				$firebase_result = json_decode($result, true);
				$checksum['result'][$increment] = $result;
				if($firebase_result['failure'] == 0)
				{

					$oNotif->update($a_firebase_data[$increment]['notif-id'],1);
			        $checksum['success']++;
					$oNotif->update($a_firebase_data[$increment]['notif-id'],$system['FIREBASE_FLAG_SEND']);
				}
				else
				{
       	 			$checksum['error']++;
		        	$checksum['error_notif_id'][] = $a_firebase_data[$increment]['notif-id'];
					$oNotif->update($a_firebase_data[$increment]['notif-id'],$system['FIREBASE_FLAG_ERROR']);
				} 
			}
			curl_close($ch);

 			$checksum['total']++;
			$increment++;
			cron_send_notification($a_firebase_data,$system,$increment,$checksum);
		}
		else
		{

			//print_r($checksum);
			//$oNotif->closeDB();	
		}

		$oNotif->closeDB();
		return $checksum;

	}
?>