package com.basar.client.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.client.R;
import com.basar.client.helper.Config;
import com.basar.client.helper.Device;
import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import libs.basar.prettydialog.PrettyDialog;
import libs.basar.prettydialog.PrettyDialogCallback;

public class SettingActivity extends BasarActivity  implements OnPermissionCallback, View.OnClickListener
{


    private static final String TAG = "setting_aktifitas";

    private  String m_error = "";

    private EditText nik,email,domain ;

    private TextView button_setting;
    private ProgressBar loading;
    public String alamat_url;

    private  PrettyDialog m_pretty_dialog;


    private PermissionHelper permission_helper;

    private boolean is_single;
    private AlertDialog alert_dialog;
    private String[] needed_permission;
    private final static String[] MULTI_PERMISSIONS = new String[]
    {
            Manifest.permission.VIBRATE,
            Manifest.permission.INTERNET,
            Manifest.permission.READ_PHONE_STATE

    };

    private  String
            device_manufucture,
            device_model,
            device_sdk,
            device_version,
            device_pixel,
            device_dpi,
            device_inch,
            device_country,
            device_language,
            device_uuid,
            device_android_id,
            device_network_carrier,
            device_network_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);


        initComponent();
        initVariable();
        initEvent();
        mainLogic();
    }

    public void onBackPressed()
    {
        return;
    }


    private void initSetting()
    {
        getDeviceInfo();
        loading.setVisibility(View.VISIBLE);
        //button_setting.setVisibility(View.GONE);
        //Toast.makeText(getApplicationContext(),"setting "+Config.getFirebaseToken(getBaseContext()),Toast.LENGTH_LONG).show();

        final String nik = this.nik.getText().toString().trim();
        final String email = this.email.getText().toString().trim();
        final String domain = this.domain.getText().toString().trim();

        alamat_url = Config.getAppProtocol()+domain+"/index.php";

        if(nik.equalsIgnoreCase("")  || email.equalsIgnoreCase( "")  || domain.equalsIgnoreCase("") )
        {
            //Toast.makeText(getApplicationContext(),"inputan harus diisi");
            loading.setVisibility(View.GONE);
            m_error = getResources().getString(R.string.setting_dialog_error_input);
            m_pretty_dialog.setIcon(R.drawable.pdlg_icon_close).setIconTint(R.color.button_danger)
                    .setIconCallback(new PrettyDialogCallback()
                    {
                        @Override
                        public void onClick()
                        {
                            // Do what you gotta do
                            m_pretty_dialog.dismiss();
                        }
                    }).setMessage(m_error).setSound(R.raw.error).showDialog();
        }
        else
        {
            //mengecek imei
            //jika ada maka update device lama dengan firebase baru
            //jika tidak ada maka buat device baru dengan firebase baru

            //generate token di firebase server
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(SettingActivity.this, new OnSuccessListener<InstanceIdResult>()
            {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult)
                {

                    final String new_token = instanceIdResult.getToken();
                    Config.setFirebaseToken(getBaseContext(),new_token);
                    Log.v(TAG,new_token);
                    //periksa apakah imei/carrierID terdaftar di server
                    Log.v(TAG,alamat_url);
                    StringRequest string_request = new StringRequest(Request.Method.POST, alamat_url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.v(TAG,response);
                                    loading.setVisibility(View.GONE);
                                    try{
                                        JSONObject json_object = new JSONObject(response);
                                        String  result = json_object.getString("status");
                                        if(result.equalsIgnoreCase("success"))
                                        {
                                            //jika imei ada maka

                                            //Config.setRegisterStatus(getBaseContext(), registerStatus);
                                            Config.setRealname(getBaseContext(), json_object.getString("real-name"));
                                            Config.setFirebaseToken(getBaseContext(),new_token);
                                            Config.setRegisterStatus(getBaseContext(), "0");
                                            Config.setDomain(getBaseContext(),domain);

                                            m_error = "";
                                            startActivity(new Intent(getBaseContext(),WaitingActivity.class));

                                        }
                                        else
                                        {
                                            //kirim register device
                                            StringRequest string_request = new StringRequest(Request.Method.POST, alamat_url,
                                                    new Response.Listener<String>() {
                                                        @Override
                                                        public void onResponse(String response) {
                                                            Log.v(TAG,response);
                                                            loading.setVisibility(View.GONE);
                                                            //Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                                                            try{
                                                                JSONObject json_object = new JSONObject(response);
                                                                String  result = json_object.getString("status");
                                                                if(result.equalsIgnoreCase("success"))
                                                                {
                                                                    Config.setRegisterStatus(getBaseContext(),"0");
                                                                    Config.setDomain(getBaseContext(),domain);
                                                                    Config.setFirebaseToken(getBaseContext(),new_token);
                                                                    m_error = "";
                                                                    startActivity(new Intent(getBaseContext(),WaitingActivity.class));
                                                                }
                                                                else
                                                                {
                                                                    m_error = json_object.getString("desc").toString();

                                                                    m_pretty_dialog.setIcon(R.drawable.pdlg_icon_close).setIconTint(R.color.button_danger)
                                                                            .setIconCallback(new PrettyDialogCallback()
                                                                            {
                                                                                @Override
                                                                                public void onClick()
                                                                                {
                                                                                    // Do what you gotta do
                                                                                    m_pretty_dialog.dismiss();
                                                                                }
                                                                            }).setMessage(m_error).setSound(R.raw.error).showDialog();
                                                                }


                                                            }catch (JSONException e){
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    },
                                                    new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {

                                                            loading.setVisibility(View.GONE);
                                                            m_error = error.toString();
                                                            m_pretty_dialog.setIcon(R.drawable.pdlg_icon_close).setIconTint(R.color.button_danger)
                                                                    .setIconCallback(new PrettyDialogCallback()
                                                                    {
                                                                        @Override
                                                                        public void onClick()
                                                                        {
                                                                            // Do what you gotta do
                                                                            m_pretty_dialog.dismiss();
                                                                        }
                                                                    }).setMessage(m_error).setSound(R.raw.error).showDialog();
                                                        }
                                                    }
                                            )
                                            {
                                                @Override
                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                    Map<String, String> params = new HashMap<>();
                                                    params.put("user-id",nik);
                                                    params.put("email",email);
                                                    params.put("app-name",Config.getAppName());
                                                    params.put("app-version",Integer.toString(Config.getVersion()));
                                                    params.put("firebase-token", Config.getFirebaseToken(getBaseContext()));
                                                    params.put("device-model", device_model);
                                                    params.put("device-sdk",device_sdk);
                                                    params.put("device-merk",device_manufucture);
                                                    params.put("device-version",device_version);

                                                    params.put("device-pixel",device_pixel);
                                                    params.put("device-dpi",device_dpi);
                                                    params.put("device-inch",device_inch);

                                                    params.put("device-country",device_country);
                                                    params.put("device-language",device_language);
                                                    params.put("device-uuid",device_uuid);

                                                    params.put("device-android-id",device_android_id);
                                                    params.put("device-network-carrier",device_network_carrier);
                                                    params.put("device-network-id",device_network_id);

                                                    params.put("page","login");
                                                    params.put("type","model");
                                                    params.put("action","register-device");

                                                    return params;

                                                }
                                            };
                                            RequestQueue request_queue = Volley.newRequestQueue(getApplicationContext());
                                            //maksimal 27 detik
                                            string_request.setRetryPolicy(new DefaultRetryPolicy(
                                                    Config.getVolleyMaxResponse()
                                                    ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                                                    ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                            request_queue.add(string_request);

                                        }


                                    }catch (JSONException e){
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    loading.setVisibility(View.GONE);
                                    m_error = getResources().getString(R.string.setting_not_connected);

                                    m_pretty_dialog.setIcon(R.drawable.pdlg_icon_close).setIconTint(R.color.button_danger)
                                            .setIconCallback(new PrettyDialogCallback()
                                            {
                                                @Override
                                                public void onClick()
                                                {
                                                    // Do what you gotta do
                                                    m_pretty_dialog.dismiss();
                                                }
                                            }).setMessage(m_error).setSound(R.raw.error).showDialog();
                                }
                            }
                    )
                    {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("device-network-carrier",device_network_id);

                            params.put("page","login");
                            params.put("type","model");
                            params.put("action","imei-check");
                            params.put("user-id",nik);
                            params.put("email",email);
                            params.put("firebase-token",new_token);

                            return params;

                        }
                    };
                    RequestQueue request_queue = Volley.newRequestQueue(getApplicationContext());
                    //maksimal 27 detik
                    string_request.setRetryPolicy(new DefaultRetryPolicy(
                            Config.getVolleyMaxResponse()
                            ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                            ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    request_queue.add(string_request);
                    //end volley request
                }
            });
        }
    }



    private  void getDeviceInfo()
    {
        Device device = new Device();
        device.init(SettingActivity.this);
        device_manufucture = device.getManufacture();
        device_model = device.getModel();
        device_sdk = String.valueOf(device.getSDKVersion());
        device_version = device.getAndroidVersion();
        device_pixel = String.valueOf(device.getScreenPixel());
        device_dpi = String.valueOf(device.getScreenDPI());
        device_inch =  String.valueOf(device.getScreenSize());
        device_country = device.getCountry();
        device_language = device.getLanguage();
        device_uuid = device.getUUID();
        device_android_id = device.getAndroidID();
        device_network_carrier = device.getNetworkCarrier();
        device_network_id = device.getNetworkID();
    }


    @Override
    protected void initComponent()
    {

        nik = findViewById(R.id.nik);
        email = findViewById(R.id.email);
        domain =  findViewById(R.id.domain);

        loading  = findViewById(R.id.loading);
        loading.setVisibility(View.GONE);
        loading.bringToFront();
        button_setting = findViewById(R.id.button_setting);

    }

    @Override
    protected  void initVariable()
    {
        m_pretty_dialog = new PrettyDialog(this);
        permission_helper = PermissionHelper.getInstance(this);
        permission_helper
                .setForceAccepting(false) // default is false. its here so you know that it exists.
                .request( MULTI_PERMISSIONS);
    }

    @Override
    protected  void  initEvent()
    {

    }

    @Override
    protected  void mainLogic()
    {
        //mengecek apakah di sharedpreference sudah ada token firebase
        if(Config.getFirebaseToken(getBaseContext())== "")
        {
            //generate token di firebase server
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SettingActivity.this,  new OnSuccessListener<InstanceIdResult>()
            {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult)
                {
                    /*
                    String new_token = instanceIdResult.getToken();
                    Config.setFirebaseToken(getBaseContext(),new_token);

                    Log.v(TAG,new_token);

                     */
                }
            });
        }
        button_setting.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){
                initSetting();
            }
        });
    }



    /*---------------begin permission helper--------------------*/
    /**
     * Used to determine if the user accepted {@link android.Manifest.permission#SYSTEM_ALERT_WINDOW} or no.
     * <p/>
     * if you never passed the permission this method won't be called.
     */
    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        permission_helper.onActivityForResult(requestCode);
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permission_helper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override public void onPermissionGranted(@NonNull String[] permissionName) {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Granted");
        //downloadDashboard();
        Log.v("PERMISSION onPermissionGranted", "Permission(s) " + Arrays.toString(permissionName) + " Granted");
    }

    @Override public void onPermissionDeclined(@NonNull String[] permissionName) {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Declined");
        Bundle bundle = new Bundle();
        bundle.putString("data_error", "memerlukan perizinan");
        Intent intent = new Intent(SettingActivity.this, ErrorActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.i("PERMISSION onPermissionDeclined", "Permission(s) " + Arrays.toString(permissionName) + " Declined");
    }

    @Override public void onPermissionPreGranted(@NonNull String permissionsName) {
        //result.setText("Permission( " + permissionsName + " ) preGranted");
        Log.i("PERMISSION onPermissionPreGranted", "Permission( " + permissionsName + " ) preGranted");
    }

    @Override public void onPermissionNeedExplanation(@NonNull String permissionName) {
        Log.i("PERMISSION NeedExplanation", "Permission( " + permissionName + " ) needs Explanation");
        if (!is_single)
        {
            needed_permission = PermissionHelper.declinedPermissions(this, MULTI_PERMISSIONS);
            StringBuilder alert_dialog = new StringBuilder(needed_permission.length);
            if (needed_permission.length > 0)
            {
                for (String permission : needed_permission)
                {
                    alert_dialog.append(permission).append("\n");
                }
            }
            //result.setText("Permission( " + alert_dialog.toString() + " ) needs Explanation");
            AlertDialog alert = getAlertDialog(needed_permission, alert_dialog.toString());
            if (!alert.isShowing())
            {
                alert.show();
            }
        }
        else
        {
            //result.setText("Permission( " + permissionName + " ) needs Explanation");
            getAlertDialog(permissionName).show();
        }
    }

    @Override public void onPermissionReallyDeclined(@NonNull String permissionName)
    {
        //result.setText("Permission " + permissionName + " can only be granted from SettingsScreen");
        Log.v("PERMISSION ReallyDeclined", "Permission " + permissionName + " can only be granted from settingsScreen");
        /** you can call  {@link PermissionHelper#openSettingsScreen(Context)} to open the settings screen */
    }

    @Override public void onNoPermissionNeeded()
    {
        //result.setText("Permission(s) not needed");
        Log.v("PERMISSION onNoPermissionNeeded", "Permission(s) not needed");
    }

    @Override public void onClick(View v)
    {

        permission_helper
                .request(Manifest.permission.SYSTEM_ALERT_WINDOW);/*you can pass it along other permissions,
                     just make sure you override OnActivityResult so you can get a callback.
                     ignoring that will result to not be notified if the user enable/disable the permission*/

    }

    public AlertDialog getAlertDialog(final String[] permissions, final String permissionName)
    {
        if (alert_dialog == null)
        {
            alert_dialog = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        alert_dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                permission_helper.requestAfterExplanation(permissions);
            }
        });
        alert_dialog.setMessage("Permissions need explanation (" + permissionName + ")");
        return alert_dialog;
    }

    public AlertDialog getAlertDialog(final String permission)
    {
        if (alert_dialog == null)
        {
            alert_dialog = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        alert_dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permission_helper.requestAfterExplanation(permission);
            }
        });
        alert_dialog.setMessage("Permission need explanation (" + permission + ")");
        return alert_dialog;
    }
    /*----------------end permission helper---------------------*/


}
