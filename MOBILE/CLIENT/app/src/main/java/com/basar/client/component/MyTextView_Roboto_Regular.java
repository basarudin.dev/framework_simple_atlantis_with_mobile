package com.basar.client.component;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView_Roboto_Regular extends TextView
{

    public MyTextView_Roboto_Regular(Context _context, AttributeSet _attrs, int _def_style)
    {
        super(_context, _attrs, _def_style);
        init();
    }

    public MyTextView_Roboto_Regular(Context _context, AttributeSet _attrs)
    {
        super(_context, _attrs);
        init();
    }

    public MyTextView_Roboto_Regular(Context _context)
    {
        super(_context);
        init();
    }

    private void init()
    {
        if (!isInEditMode())
        {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Regular.ttf");
            setTypeface(tf);
        }
    }

}